# Readme for the tool Bviewer

1. Read bview-doc.pdf for the definition of the language Bview and the
scope and limitations of this tool.
2. This version runs in guile-scheme, just checked for version 1.8
3. Start guile. (If you do this from within emacs, you will have to ensure that
   the variable 'scheme-program-name' in scheme-mode is defined to be "guile"
4. As described in bview-doc.pdf, at the guile prompt:

        guile> (load "library.scm")
        guile> (load-spec-file "elf.spec")
        guile> (data-read "test.o")

    should display the s-expression corresponding to the given elf
    spec. In this example, the section-table and elf-header should show up.

5. You could reload and try with `wav.spec` and the corresponding
sample `new.wav`. Try this outside emacs since this is a huge wav sample.


6. No error-handling in this version, so you need to be careful and
 compassionate with this code ;-)

7. The language here is fairly general, but the implementation is a
 skeletal prototype and hopefully should get into an environment like
 Haskell. But I still think that scheme's mix of paradigmns
 (imperative and declarative and typelessness have been exploited here
 quite a bit and a strong-typed fully functional enviroment may not be
 as neat.

8. This is still alpha code. Use at your risk!

