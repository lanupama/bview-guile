;------------------------------------------------------------------------------
; elf.spec
;-----------------------------------------------------------------------------
((set-val galign 0)
 (def-type uint (seq byte (length 4) (rep unsigned)))
 (def-type astring (seq char (length 3) (rep ascii)))
 (def-type uhalf (seq byte (length 2) (rep unsigned)))

 (def-type header-struct 
   (struct (
	    (e_ident0     byte)
	    (e_ident1     astring)
	    (e_ident2     uint)
	    (e_ident3     uint)
	    (e_ident4     uint)
	    (e_type       uhalf)
	    (e_machine    uhalf) 
	    (e_version    uint) 
	    (e_entry      uint) 
	    (e_phoff      uint) 
	    (e_shoff      uint)     
	    (e_flags      uint) 
	    (e_ehsize     uhalf)
	    (e_phentsize  uhalf)
	    (e_phnum      uhalf)
	    (e_shentsize  uhalf)
	    (e_shnum      uhalf)
	    (e_shstrndx   uhalf))))


 (def-type section-header-struct 
   (struct ((sh_name      uint)
	    (sh_type      uint)
	    (sh_flags     uint)
	    (sh_addr      uint)
	    (sh_offset    uint)
	    (sh_size      uint)
	    (sh_link      uint)
	    (sh_info      uint)
	    (sh_addralign uint)
	    (sh_entsize   uint))))

 (def-object elf-header (type header-struct) (offset 0) (whence 0))



(def-object elf-section-table 
  (type (seq section-header-struct 
          (length (struct-ref elf-header e_shnum)) 
                                (rep default)))
	    (offset (struct-ref elf-header e_shoff)) 
	    (whence 0)))



