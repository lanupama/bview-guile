;binread.scm:  All supporting Readers generated/implemented here
;TODO alignment and endianness support very flaky
;

;Extracting the only optional argument from  a vararg function
;---------------------------------------------------------------------

(define (get-optarg l)
  (if (null? l) l (car l)))



;Adjusting alignment
;---------------------------------------------------------------------

(define (adjust-align align)
  (let* ((nn    (* 2 align))
	 (m     (if (= nn 0) 0 (modulo goff nn))))
       (read-type read-byte-new m)))



; Evaluating expressions which are part of our type-language
;---------------------------------------------------------------------

(define (eval-exp l . c)
 (let ((context (get-optarg c)))
   (destruct l
     (('struct-ref x y)    (assoc-ref 
		                  (if (symbol? x)
				      ((assoc-ref object-readers x))
				      (eval-exp x context))  y))
     (('structSelf-ref x)   (assoc-ref context x))
     (('array-ref x y)      (lambda ()
			        (let ((l (if (symbol? x)
				 	     ((assoc-ref object-readers x))
					     (eval-exp x context))))
			       (getval l y))))
     (('= y)                (cequal?  y))
     ((f '#t)               f)
     ((f '#f)               (composeo f not))
     ((op x y)              ((assoc-ref op-list op)
                            (eval-exp x context) (eval-exp y context)))
     (('existsif y)         (eval-exp y context))
     (x                     x))))


; Getting the corresponding type-reader
;---------------------------------------------------------------------------
(define (get-type-reader arg c)
    (if (symbol? arg)
	(assoc-ref type-readers arg)
	(interpret-type arg c)))



;Auxiliary functions for reading
;------------------------------------------------------------------------------
(define (modify-goff new-val)
    (set! goff new-val))

(define (make-list base-reader delim-func)
  (let f ((c (base-reader)))
	  (if (or (delim-func c)  (eof-object? c)) '()
	      `(,c . ,(f (base-reader))))))


;Primitive Readers
;-----------------------------------------------------------------------------
(define (read-bytes count)
  (let* ((goff-to    (+ goff   count ))
         (res        (string->list (substring gbuf goff goff-to)))
         (x          (modify-goff goff-to)))
         res))

(define (read-byte-new)
   (let* ((res (string-ref gbuf goff))
	  (x   (modify-goff (+ 1 goff))))
       (if (eof-object? res) 
	   res
	   (char->integer res))))

(define (read-char-new)
;        (compose read-byte-new integer->char))
   (let ((x (read-byte-new)))
       (if (eof-object? x ) x (integer->char x))))

;-----------------------------------------------------------------------------

(define (read-type base-reader count)
  (let ((str '()) (z count))
       (while (> z 0 )
	   (begin  (let  ((c1 (base-reader)))
	           (set! str `(,c1 . ,str))
	           (set! z (- z 1)))))
       str))





;1) Fixed Length Reader (Reader for fixed length type)
;---------------------------------------------------------------------------
(define ((fixed-length-reader base-type length-exp rep align c))
  (let* ((elength (eval-exp length-exp c))
	 (m (adjust-align align))
	 (l (read-type (assoc-ref type-readers base-type) elength)))
    ((assoc-ref interpret-functions rep) (reverse l))))

;Function eval-exp evaluates length-exp to yield an integer. Adjust-align 
;uses an alignment parameter which overrides global to re-align



;2) Struct Reader (added to type-reader)
;----------------------------------------------------------------------------
(define ((struct-reader l align c))
 (let* ((m   (adjust-align align))
        (read-member
	  (lambda (c1 x) (let* ((type-reader (get-type-reader (cadr x) c1))
	                        (x2          (cddr x)))
		                 (if (or (null? x2)
				        (eq? (eval-exp (car x2) c1) #t))
				  `(,@c1 ,(cons (car x) (type-reader)))
				   c1)))))
   ((drop (length c)) (foldl read-member c l))))



;3) delimited types (added to type-reader)
;----------------------------------------------------------------------------

(define ((delim-reader base-type delim-exp rep align c))
 (let* ((m (adjust-align align))
	(l (make-list (assoc-ref type-readers base-type)
		       (eval-exp delim-exp  c))))
   ((assoc-ref interpret-functions rep) l)))



;4) Union types
;----------------------------------------------------------------------------

(define ((union-reader t l align  c))
  (let* ((m            (adjust-align align))
         (tag-val      ((get-type-reader t c)))
         (type-reader  (get-type-reader (assoc-ref l tag-val) c)))
    (cons tag-val (type-reader))))




;Interpret functions for <rep>. Supported are unsigned,signed,ascii,default
;---------------------------------------------------------------------------

(define (make-unsigned l)
  (let loop ((b l) (acc 0) (i 0))
    (if (null? b) acc
	(loop (cdr b) (+ (* (car b) (expt 256 i)) acc) (+ i 1)))))

; For ascii/text strings
(define make-ascii list->string)

; Do nothing
(define make-default id)



;Object-reader (added to object-reader)
;----------------------------------------------------------------------------
(define ((make-object-reader type offset from))
  (let* ((eval-off     (eval-exp offset))
         (offset1      (modify-goff eval-off))
	 (type-reader  (get-type-reader type '())))
     (type-reader)))
         


; Specification file-reader 
;----------------------------------------------------------------------------

(define spec-file-reader  read)
;   ((fixed-length-reader 's-exp 4 'default 0)))



;Reading the actual binary data file
;----------------------------------------------------------------------------
(define (data-read filename)
 (begin (read-string!/partial gbuf (open-input-file filename))
        (all-object-reader)))
;-----------------------------------------------------------------------------
