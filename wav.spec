;(set-val galign 1)

((def-type uint     (seq byte (length 4) (rep unsigned)))
 (def-type uhalf    (seq byte (length 2) (rep unsigned)))
 (def-type mystring (seq char (length 4) (rep ascii)))


(def-type fmt-chunk (struct ((size    uint) 
	                     (ccode   uhalf) 
			     (chnl    uhalf)
	                     (srate   uint) 
			     (abytes  uint) 
			     (blockal uhalf) 
			     (sbits   uhalf)
			     (fbytes  uhalf))))
;	                     (lbytes  (seq byte (length 
;;					            (structSelf-ref fbytes))
;;				                (rep default))))
						    

(def-type data-chunk 
            (struct ((size uint)
	             (data (seq byte (length 
				        (structSelf-ref size))
				     (rep default)))
		     )))

(def-type fact-chunk 
            (struct ((size uint)
	             (data (seq byte (length 
				        (structSelf-ref size))
				     (rep default))))))


;(def-type chunk-union (union (("fmt " . fmt-chunk) ("data" . data-chunk)
;	                      ("fact" . fact-chunk))))

(def-type chunk-union 
              (union (tagType (seq char (length 4) (rep ascii)))
	                     (("fmt " . fmt-chunk)
			      ("data" . data-chunk)
			      ("fact" . fact-chunk))))


(def-type wav (struct ((id          mystring)
	               (size        uint)
		       (riff-type   mystring)
		       (wave-chunks 
			 (seq chunk-union (length 3)
				                 (rep default))))))

;(def-type wav (struct ((id          mystring)
;	               (size        uint)
;		       (riff-type   mystring))))

(def-object wav-obj (type wav) (offset 0) (whence 0)))

