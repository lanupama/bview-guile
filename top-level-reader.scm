;top-level-reader.scm: Top level code for reading a spec file and generating its
;corresponding binary readers.
;---------------------------------------------------------------------------
;This file has the code to load an arbitrary specification file description 
;and generate the corresponding readers for parsing files of that specification
;type. For eg an elf file spec will generate elf readers, a wav file spec will 
;generate a wav file reader etc
;
;(load-spec-file file-name) is a top level call to generate such a
;reader 
;(data-read filename) reads data files of the corresponding
;type and parses them to generate s-expressions according to the
;spec-file description 
;This version is not
;parametric on readers and can handle only one spec file at a
;time. Through a small modification, top level dictionaries can be
;generated for simultaneously managing multiple file types.



(define (load-spec-file file-name)
    (let ((spec-list (with-input-from-file file-name spec-file-reader)))
               (for-each interpret spec-list)))

; map interpret on the specification list of top-level types to get
; the corresponding base-type-reader

;Map input structure to a corresponding reader-maker

(define (interpret-type z . c)
 (let ((c2 (get-optarg c)))
    (destruct z
      (('struct b )                  (struct-reader b galign c2))
      (('struct b ('align al) )      (struct-reader b al c2))
      (('seq b ('length d) (rep e))  (fixed-length-reader b d e galign c2))
      (('seq b ('length d) (rep e) ('align al))
                                     (fixed-length-reader b d e al c2))
      (('seq b ('delim  d) (rep e))  (delim-reader b d e galign c2))
      (('seq b ('delim  d) (rep e) ('align al))  
                                     (delim-reader b d e al c2))
      (('union (tagType t) l)        (union-reader t l galign c2))
      (('union (tagType t) l ('align al)) 
                                     (union-reader t l al c2))
      ( x x))))


; Interpret the specification which is an s-exp and add the
; corresponding readers to top level data-structures ie. type-readers
; and object-readers

(define (interpret l)
 (destruct l
   (('def-type y z) (set! type-readers 
			 (assoc-set! type-readers y (interpret-type z))))

   (('def-object w (type x) (offset y) (whence z))
      (set! object-readers 
	  (assoc-set! object-readers w (make-object-reader 
					    (interpret-type x) y z))))

   (('set-val 'galign x)  (set! galign x))
		
   (('set-val 'endian x)  (set! endian x))))



