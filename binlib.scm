;------------------------------------------------------------------------------
; binlib.scm 
;Some usual libraries, not available in Scheme
;Need to re-write to use destruct instead of car and cdr and cons.
;
;------------------------------------------------------------------------------
(define (pam l arg)
        (if (null? l)
         l
         (cons ((car l) arg) (pam (cdr l) arg))))

(define id (lambda (x) x))

(define (foldl f id l)
   (if (null? l)
      id 
   (foldl f (f id (car l)) (cdr l))))

(define (bincompose f g)
   (lambda (x)
      (g (f x ))))

(define composeo 
  (lambda l
    (foldl  bincompose  (lambda (x) x) l )))

(define (foldl f id l)
   (if (null? l)
      id 
   (foldl f (f id (car l)) (cdr l))))

(define (bincompose f g)
   (lambda (x)
      (g (f x ))))

(define (curryo f) (lambda (x) (lambda (y) (f x y ))))

(define (zip-pair l  m) 
         (if (or (null? l) (null? m)) '()
             (cons (cons (car l) (car m)) (zip-pair (cdr l) (cdr m)))))

(define (getval l pos)
   (if (= pos 1) (car l)
       (getval (cdr l) (- pos 1))))

(define cassoc-ref (curryo assoc-ref))
(define cequal? (curryo equal?))

(define (repeat x length)
      (let loop ((l length) (s '()))
           (if (= l 0) s
               (loop (- l 1) (cons x s)))))

; Set! the scheme way instead of 2 reverses
(define (dropLast n) (compose reverse (drop n) reverse))
(define (new-apply f)
         (apply f '()))


;-----------------------------------------------------------------------------
;End of binlib.scm
;-----------------------------------------------------------------------------
