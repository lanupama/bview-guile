;Spec-file globals
; The global variables that are populated when a file-spec is read and
; interpreted


(define gendian 'little)
(define galign  1)
(define endian 'little)



(define type-readers
  `(
    (char . ,read-char-new)
    (byte . ,read-byte-new)
    (s-exp . ,read)))


(define interpret-functions 
 `((unsigned . ,make-unsigned) (default . ,make-default) 
   (ascii . ,make-ascii)))

(define op-list
  `((+ . ,+) (- . ,-) (* . ,*) (/ . ,/) (expt . ,expt) (= . ,equal?)))

(define object-readers '())                                                

(define  (all-object-reader)
   (zip-pair (map car object-readers) 
	     (map (compose cdr new-apply) object-readers)))   


;Data File Globals
;Global variables that track and delimit the file data reads -- file offsets
;---------------------------------------------------------------------

(define goff 0)
(define gstart 0);for future use in general buffers
(define gend   0);

(define gbuf (make-string 1569000))
